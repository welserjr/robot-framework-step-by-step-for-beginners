# Robot Framework Step by Step for Beginners

If you want to learn Robot Framework from scratch, this course is for you. Very basic step by step video guides to take you from knowing nothing on Robot Framework to developing a project for Test Automation.

## Resources
* https://robotframework.org/
* https://github.com/robotframework/SeleniumLibrary/
* https://tecadmin.net/setup-selenium-chromedriver-on-ubuntu/

## Setup & Teardown
* TC SetUp - run before every TC
* TC Teardown - will run after every TC
* TS Setup - will run before TS
* TS Teardown - will run after TS